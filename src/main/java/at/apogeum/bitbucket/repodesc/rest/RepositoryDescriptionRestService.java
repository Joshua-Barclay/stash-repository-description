package at.apogeum.bitbucket.repodesc.rest;

import at.apogeum.bitbucket.repodesc.JGitUtil;
import at.apogeum.bitbucket.repodesc.config.RepoConfig;
import at.apogeum.bitbucket.repodesc.config.RepoConfigPersistenceManager;
import at.apogeum.bitbucket.repodesc.rest.data.DescriptionsData;
import at.apogeum.bitbucket.repodesc.rest.data.RepositoryData;
import com.atlassian.applinks.api.ApplicationLink;
import com.atlassian.applinks.api.ApplicationLinkService;
import com.atlassian.applinks.api.application.jira.JiraApplicationType;
import com.atlassian.bitbucket.auth.AuthenticationContext;
import com.atlassian.bitbucket.permission.PermissionService;
import com.atlassian.bitbucket.repository.RepositoryService;
import com.atlassian.bitbucket.server.ApplicationPropertiesService;
import com.atlassian.bitbucket.user.ApplicationUser;
import com.atlassian.plugin.spring.scanner.annotation.export.ExportAsService;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import com.atlassian.plugins.rest.common.security.AnonymousAllowed;
import com.atlassian.sal.api.component.ComponentLocator;

import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.io.IOException;
import java.sql.SQLException;
import java.util.List;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.QueryParam;

/**
 * @author Stephan Bechter <stephan@apogeum.at>
 */
@ExportAsService({RepositoryDescriptionRestService.class})
@Path("/")
public class RepositoryDescriptionRestService {

    @ComponentImport
    private final RepoConfigPersistenceManager repoConfigPersistenceManager;

    @ComponentImport
    private final RepositoryService repositoryService;

    @ComponentImport
    private final PermissionService permissionService;

    @ComponentImport
    private final ApplicationPropertiesService applicationPropertiesService;

    @ComponentImport
    private final AuthenticationContext authenticationContext;
    
    @ComponentImport
    private final ApplicationLinkService applicationLinkService;


    @Inject
    public RepositoryDescriptionRestService(RepoConfigPersistenceManager repoConfigPersistenceManager,
                                            RepositoryService repositoryService, PermissionService permissionService,
                                            ApplicationPropertiesService applicationPropertiesService,
                                            AuthenticationContext authenticationContext,
                                            ApplicationLinkService applicationLinkService) {
        this.repoConfigPersistenceManager = repoConfigPersistenceManager;
        this.repositoryService = repositoryService;
        this.permissionService = permissionService;
        this.applicationPropertiesService = applicationPropertiesService;
        this.authenticationContext = authenticationContext;
        this.applicationLinkService = applicationLinkService;
    }

    @GET
    @AnonymousAllowed
    @Produces({MediaType.APPLICATION_JSON})
    @Path("/projects/{projectKey}")
    public Response getDescriptionsForProject(@PathParam("projectKey") String projectKey,
                                              @DefaultValue("0") @QueryParam("start") int start,            
                                              @DefaultValue("100") @QueryParam("limit") int limit) throws IOException {
        final DescriptionsData descriptions = new DescriptionsData();
        descriptions.setJiraLink(getJiraLink());
        try {
            List<RepoConfig> configs = repoConfigPersistenceManager.getRepositoryConfigurationsForProjectKey(projectKey, start, limit);
            ApplicationUser currentUser = authenticationContext.getCurrentUser();
            List<RepositoryData> result = JGitUtil.joinWithCommitData(
                    repositoryService, permissionService, applicationPropertiesService, currentUser, configs
            );
            for (RepositoryData data : result) {
                descriptions.addRepoData(data);
            }
            return Response.ok(descriptions).build();
        } catch (SQLException e) {
            return Response.serverError().build();
        }
    }

    @GET
    @AnonymousAllowed
    @Produces({MediaType.APPLICATION_JSON})
    @Path("/public")
    public Response getDescriptionsForPublic(@DefaultValue("0") @QueryParam("start") int start,            
                                             @DefaultValue("100") @QueryParam("limit") int limit) throws IOException {
        final DescriptionsData descriptions = new DescriptionsData();
        descriptions.setJiraLink(getJiraLink());
        try {
            List<RepoConfig> configs = repoConfigPersistenceManager.getRepositoryConfigurationsForPublicProjects(start, limit);
            ApplicationUser currentUser = authenticationContext.getCurrentUser();
            List<RepositoryData> result = JGitUtil.joinWithCommitData(
                    repositoryService, permissionService, applicationPropertiesService, currentUser, configs
            );
            for (RepositoryData data : result) {
                descriptions.addRepoData(data);
            }
            return Response.ok(descriptions).build();
        } catch (SQLException e) {
            return Response.serverError().build();
        }
    }

    @GET
    @AnonymousAllowed
    @Produces({MediaType.APPLICATION_JSON})
    @Path("/private/{userId}")
    public Response getDescriptionsForPrivate(@PathParam("userId") String userId,
                                              @DefaultValue("0") @QueryParam("start") int start,            
                                              @DefaultValue("100") @QueryParam("limit") int limit) throws IOException {
        final DescriptionsData descriptions = new DescriptionsData();
        descriptions.setJiraLink(getJiraLink());
        try {
            List<RepoConfig> configs = repoConfigPersistenceManager.getRepositoryConfigurationsForPrivateProjects(userId, start, limit);
            ApplicationUser currentUser = authenticationContext.getCurrentUser();
            List<RepositoryData> result = JGitUtil.joinWithCommitData(
                    repositoryService, permissionService, applicationPropertiesService, currentUser, configs
            );
            for (RepositoryData data : result) {
                descriptions.addRepoData(data);
            }
            return Response.ok(descriptions).build();
        } catch (SQLException e) {
            return Response.serverError().build();
        }
    }

    private String getJiraLink() {
        String jiraUrl = null;
        ApplicationLink jiraLink = applicationLinkService.getPrimaryApplicationLink(JiraApplicationType.class);
        if (jiraLink != null) {
            jiraUrl = jiraLink.getDisplayUrl().toString();
            if (jiraUrl.endsWith("/")) {
                jiraUrl = jiraUrl.substring(0, jiraUrl.length() - 1);
            }
        }
        return jiraUrl;
    }
}
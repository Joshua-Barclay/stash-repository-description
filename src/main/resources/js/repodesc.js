define('plugin/repodesc/default', [
    'jquery',
    'bitbucket/util/navbuilder',
    'bitbucket/util/events'
], function ($, nav, events) {

    // http://stackoverflow.com/questions/9229645/remove-duplicates-from-javascript-array/9229821
    function uniq(a) {
        return a.sort().filter(function (item, pos, ary) {
            return !pos || item != ary[pos - 1];
        })
    }

    function extractIds(s) {
        function reverse(s) {
            for (var i = s.length - 1, o = ''; i >= 0; o += s[i--]) {
            }
            return o;
        }

        var jira_matcher = /\d+-[A-Z]+(?!-?[a-zA-Z]{1,10})/g;
        var r = reverse(s);
        var matches = r.match(jira_matcher);
        if (!matches) {
            matches = []
        }
        for (var j = 0; j < matches.length; j++) {
            var m = reverse(matches[j]);
            matches[j] = m.replace(/-0+/, '-'); // trim leading zeroes:  ABC-0123 becomes ABC-123
        }

        // need to remove duplicates, since they will cause n^2 links to be created (n = dups).
        return uniq(matches);
    }

    function toDate(str) {
        var dateOptionsLong = {year: 'numeric', month: 'short', day: 'numeric', hour: 'numeric', minute: 'numeric'};
        var val = parseInt(str, 10);
        if (!isNaN(val)) {
            var time = new Date(val);
            return '<time datetime="' + time.toISOString() + '" data-unixtime="' + str + '">' + time.toLocaleDateString(undefined, dateOptionsLong) + '</time>';
        }
        return '';
    }

    /**
     * Called once as early as possible to initialise the plugin.
     * @param config - the configuration for this module.
     */
    function initialise(config) {

        var $repoTable = ($("#repositories-container table").length ? $("#repositories-container table") : $("#repository-container table"));
        if (!$repoTable.length) {
            // try to get table for BBServer prior 5.5
            $repoTable = $("#repositories-table");
        }
        if (!$repoTable.length) return;
        var baseUrl = nav.rest('repodesc').build();
        var dataUrl;

        switch (config.visibility) {
            case "Public":
                dataUrl = baseUrl + "/public";
                break;
            case "Private":
                var userSlug = $(".aui-avatar.aui-avatar-xxlarge.user-avatar").attr("data-username");
                dataUrl = baseUrl + "/private/" + userSlug;
                break;
            default:
                var projectSlug = location.pathname.substring(location.pathname.lastIndexOf("/") + 1);
                dataUrl = baseUrl + "/projects/" + projectSlug;
        }

        $repoTable.addClass("aui-table-sortable");
        $repoTable.find("thead tr:nth-child(1)").append("<th>" + AJS.I18n.getText("repodesc.description.label") + "</th><th>Most Recent Commit</th><th>Author</th><th>Date</th>");
        $.ajax({
            url: dataUrl,
            type: "GET",
            dataType: "json",
            success: getDescriptionLoadedCallback(dataUrl)
        });
    }

    /**
     * This is a factory that generates an AJAX callback function to be used on plugin initialisation.
     *
     * @param dataUrl The URL to use to use to call the REST service for repository description data.
     *    This will only be used if the REST service provides paginated results.
     * @returns {Function} An AJAX callback which handles the initial fetch of the repository descriptions.
     * The response may contain descriptions for ALL repositories or it may be only the first "page" if future versions of
     * the REST service enable pagination.
     */
    function getDescriptionLoadedCallback(dataUrl) {
        var jira;
        var repositories;

        $(document).ajaxSuccess(ajaxHandler);

        function ajaxHandler(event, xhr, settings) {
            var response = xhr.responseJSON;
            if (settings.url.indexOf("rest/api/latest/projects") !== 0 && xhr.status === 200 && response) {
                updateNewPage(response);
            }
        }

        /**
         * Adds repository description and other information about an individual repository in the repositories table.
         * @param repoData The repository information to add to the table.
         * @param idx The index of the row to update in the repository table.
         */
        function addRepoData(repoData, idx) {
            var $repoTable = ($("#repositories-container table").length ? $("#repositories-container table") : $("#repository-container table"));
            if (!$repoTable.length) {
                // try to get table for BBServer prior 5.5
                $repoTable = $("#repositories-table");
            }
            var index = idx + 1;  // CSS selector index is one base, idx is zero base
            var msg = repoData.lastUpdateMessage;
            if (jira) {
                var ids = extractIds(msg);
                for (var j = 0; ids && ids.length && j < ids.length; j++) {
                    id = ids[j];
                    msg = msg.replace(
                        new RegExp(id, 'g'),
                        "<a class='jira-issues-trigger' data-single-issue='true' data-issue-keys='" + id + "' href='" + jira + "/browse/" + id + "'>" + id + "</a>");
                }
            }
            var html = "<td class='repodesc' title='" + repoData.descriptionFull + "'>" + repoData.description + "</td>";
            html += "<td class='repodesc repodesc_commit' title='" + repoData.lastUpdateMessageFull + "'>" + msg + "</td>";
            html += "<td class='repodesc_commit'>" + repoData.lastAuthor + "</td>";
            html += "<td class='repodesc_commit'>" + toDate(repoData.lastUpdateTime) + "</td>";
            var row = $repoTable.find("tbody tr:nth-child(" + index + ")");
            row.append(html);
            row.find('td.repodesc').tooltip({delayIn: 200});
        }

        /**
         * Handles pagination events.
         * Under some circumstances it may be desirable to call this function elsewhere.
         * @param data The pagination payload.
         */
        function updateNewPage(data) {
            var addRepoPage = function(offset, repos) {
                    for (var i = 0; i < repos.length; i++) {
                        addRepoData(repos[i], i + offset);
                    }
                };
            if (data && data.start) {
                if (repositories && repositories.length > data.start) {
                    // "old" mode where the repository data is not paginated, we already have it all
                    addRepoPage(data.start, repositories.slice(data.start));
                } else {
                    // it's the new style, paginated repo data
                    repositories = null;  // don't need to store these in the paginated scenario
                    $.ajax({
                        url: dataUrl + '?start=' + data.start + '&limit=' + data.limit,
                        type: "GET",
                        dataType: "json",
                        success: function (response) {
                            jira = response.jiraLink;
                            addRepoPage(data.start, response.repositories);
                            // applyTableSort();
                            // initJiraIntegration();  // TODO does this need to be called when we add new rows?
                        }
                    });
                }
            }
        }

        /**
         * AJAX callback which handles the initial fetch of the repository descriptions.
         * The response may contain descriptions for ALL repositories or it may be only the first "page" if future versions of
         * the REST service enable pagination.
         */
        return function(response) {
            jira = response.jiraLink;
            repositories = response.repositories;
            repositories.forEach(addRepoData);
            applyTableSort();
            initJiraIntegration();
        };
    }

    /**
     * Wire up table sorting to the repository table.
     */
    function applyTableSort() {
        var $repoTable = ($("#repositories-container table").length ? $("#repositories-container table") : $("#repository-container table"));
        if (!$repoTable.length) {
            // try to get table for BBServer prior 5.5
            $repoTable = $("#repositories-table");
        }
        $repoTable.tablesorter({
            textExtraction: function (node) {
                var nft = (node.firstChild && node.firstChild.tagName) || undefined;
                if (nft && nft.toUpperCase() === 'TIME') {
                    return node.firstChild.getAttribute('data-unixtime');
                } else {
                    return $(node).text();
                }
            }
        });
        AJS.tablessortable.setTableSortable($repoTable);
    }

    /**
     * Initialises JIRA integration.
     */
    function initJiraIntegration() {
        require(['bitbucket/internal/util/ajax'], function (ajax) {
            jiraIntegration.triggers.init({
                id: 'commits-jira-issues-dialog',
                selector: '.jira-issues-trigger'
            }, {
                ajax: function (options) {
                    return ajax.rest($.extend({
                        statusCode: {
                            200: false,
                            500: false
                        }
                    }, options));
                }
            });
        });
    }

    return { initialise: initialise };
});

package at.apogeum.bitbucket.repodesc.managers;

import com.atlassian.bitbucket.repository.Repository;
import com.atlassian.bitbucket.repository.RepositoryService;
import com.atlassian.bitbucket.util.Page;
import com.atlassian.bitbucket.util.PageRequestImpl;
import com.google.common.collect.ImmutableList;
import junit.framework.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class RepositoryManagerTest {

    public static final String TEST_PROJECT = "test-project";
    public RepositoryManager repositoryManager;

    @Before
    public void setUp() {
        RepositoryService repositoryService = mock(RepositoryService.class);

        final Page page = mock(Page.class);

        Repository repo1 = newRepo(1, false);
        Repository repo2 = newRepo(2, true);
        Repository repo3 = newRepo(3, true);
        doReturn(ImmutableList.<Repository>of(repo1, repo2, repo3))
                        .when(page).getValues();

        when(repositoryService.findByProjectKey(TEST_PROJECT, new PageRequestImpl(0, 100))).thenReturn(page);
        when(repositoryService.findAll(new PageRequestImpl(0, 100))).thenReturn(page);

        repositoryManager = new RepositoryManager(repositoryService);
    }

    private Repository newRepo(int id, boolean b) {
        Repository repo = mock(Repository.class);
        when(repo.getId()).thenReturn(id);
        when(repo.isPublic()).thenReturn(b);
        return repo;
    }

    @Test
    public void testFindByProjectKey() throws Exception {
        final List<Repository> repos = new ArrayList<Repository>();

        repositoryManager.findByProjectKey(TEST_PROJECT,0, 100, new RepositoryCallback() {
            @Override
            public void handle(Repository repository) {
                repos.add(repository);
            }
        });
        Assert.assertEquals(3, repos.size());
        Assert.assertEquals(1, repos.get(0).getId());
        Assert.assertEquals(2, repos.get(1).getId());
        Assert.assertEquals(3, repos.get(2).getId());
    }

    @Test
    public void testFindPublicProjects() throws Exception {
        final List<Repository> repos = new ArrayList<Repository>();

        repositoryManager.findPublic(0, 100, new RepositoryCallback() {
            @Override
            public void handle(Repository repository) {
                repos.add(repository);
            }
        });
        Assert.assertEquals(2, repos.size());
        Assert.assertEquals(2, repos.get(0).getId());
        Assert.assertEquals(3, repos.get(1).getId());
    }
}
package at.apogeum.bitbucket.repodesc.rest.data;

import org.junit.Assert;
import org.junit.Test;

public class RepositoryDataTest {

    @Test
    public void testBasic() {
        RepositoryData d = new RepositoryData(
                "name", "description", "lastUpdateTime",
                "lastUpdateMessage", "lastUpdateMessageFull", "lastAuthor"
        );
        Assert.assertEquals("name", d.getName());
        Assert.assertEquals("description", d.getDescription());
        Assert.assertEquals("description", d.getDescriptionFull());
        Assert.assertEquals("lastUpdateTime", d.getLastUpdateTime());
        Assert.assertEquals("lastUpdateMessage", d.getLastUpdateMessage());
        Assert.assertEquals("lastUpdateMessageFull", d.getLastUpdateMessageFull());
        Assert.assertEquals("lastAuthor", d.getLastAuthor());
    }


    @Test
    public void testTruncation() {
        RepositoryData d = new RepositoryData(
                "name",
                "description_this_one_is_very_very_long_and_ends_up_being_exactly_eighty_eight_characters",
                "lastUpdateTime",
                "lastUpdateMessageFull_this_one_is_also_very_long_and_ends_up_being_exactly_ninety_nine_characters_",
                "lastUpdateMessageFull_this_one_is_also_very_long_and_ends_up_being_exactly_ninety_nine_characters_ with some extras :-)",
                "lastAuthor"
        );
        Assert.assertEquals("name", d.getName());
        Assert.assertEquals("description_this_one_is_very_very_long_and_ends_up_being_exactly_eighty_eight_ch...", d.getDescription());
        Assert.assertEquals("description_this_one_is_very_very_long_and_ends_up_being_exactly_eighty_eight_characters", d.getDescriptionFull());
        Assert.assertEquals("lastUpdateTime", d.getLastUpdateTime());
        Assert.assertEquals("lastUpdateMessageFull_this_one_is_also_very_long_and_ends_up_being_exactly_ninet...", d.getLastUpdateMessage());
        Assert.assertEquals("lastUpdateMessageFull_this_one_is_also_very_long_and_ends_up_being_exactly_ninety_nine_characters_ with some extras :-)", d.getLastUpdateMessageFull());
        Assert.assertEquals("lastAuthor", d.getLastAuthor());
    }

    @Test
    public void testHtmlEntities() {
        RepositoryData d = new RepositoryData(
                "name",
                "description with <b> and 'quote' and \"quotes\"",
                "lastUpdateTime",
                "lastUpdateMessage <b> and 'quote' and \"quotes\"",
                "lastUpdateMessageFull & co and &amp; and co ",
                "lastAuthor"
        );
        Assert.assertEquals("name", d.getName());
        Assert.assertEquals("description with &lt;b&gt; and &apos;quote&apos; and \"quotes\"", d.getDescription());
        Assert.assertEquals("description with &lt;b&gt; and &apos;quote&apos; and \"quotes\"", d.getDescriptionFull());
        Assert.assertEquals("lastUpdateTime", d.getLastUpdateTime());
        Assert.assertEquals("lastUpdateMessage &lt;b&gt; and &apos;quote&apos; and \"quotes\"", d.getLastUpdateMessage());
        Assert.assertEquals("lastUpdateMessageFull &amp; co and &amp;amp; and co ", d.getLastUpdateMessageFull());
        Assert.assertEquals("lastAuthor", d.getLastAuthor());
    }


}

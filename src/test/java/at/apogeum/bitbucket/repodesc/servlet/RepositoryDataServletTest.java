package at.apogeum.bitbucket.repodesc.servlet;

import at.apogeum.bitbucket.repodesc.MockHttpServletResponce;
import at.apogeum.bitbucket.repodesc.config.RepoConfig;
import at.apogeum.bitbucket.repodesc.config.RepoConfigPersistenceManager;
import com.atlassian.bitbucket.permission.Permission;
import com.atlassian.bitbucket.permission.PermissionValidationService;
import com.atlassian.bitbucket.repository.Repository;
import com.atlassian.bitbucket.repository.RepositoryService;
import com.atlassian.soy.renderer.SoyException;
import com.atlassian.soy.renderer.SoyTemplateRenderer;
import com.atlassian.webresource.api.assembler.PageBuilderService;
import com.atlassian.webresource.api.assembler.RequiredResources;
import com.atlassian.webresource.api.assembler.WebResourceAssembler;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;
import java.sql.SQLException;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class RepositoryDataServletTest {

    HttpServletRequest mockRequest;
    HttpServletResponse mockResponse;
    private RepositoryDataServlet servlet;

    @Before
    public void setup() throws SoyException, SQLException, ServletException, IOException {
        mockRequest = mock(HttpServletRequest.class);
        mockResponse = new MockHttpServletResponce();

        when(mockRequest.getPathInfo()).thenReturn("/test-project/test-repo");

        SoyTemplateRenderer soyTemplateRenderer = mock(SoyTemplateRenderer.class);
        doNothing().when(soyTemplateRenderer).render(any(Appendable.class), anyString(), anyString(), any(Map.class));

        PageBuilderService pageBuilderService = mock(PageBuilderService.class);
        WebResourceAssembler webResourceAssembler = mock(WebResourceAssembler.class);
        RequiredResources requiredResources = mock(RequiredResources.class);
        when(requiredResources.requireWebResource(anyString())).thenReturn(null);
        when(webResourceAssembler.resources()).thenReturn(requiredResources);
        when(pageBuilderService.assembler()).thenReturn(webResourceAssembler);

        Repository repo = mock(Repository.class);
        when(repo.getId()).thenReturn(123);

        RepositoryService repositoryService = mock(RepositoryService.class);
        when(repositoryService.getBySlug("test-project", "test-repo")).thenReturn(repo);

        PermissionValidationService permissionValidationService = mock(PermissionValidationService.class);
        doNothing().when(permissionValidationService).validateForRepository(repo, Permission.REPO_ADMIN);

        RepoConfigPersistenceManager repoConfigPersistenceManager = mock(RepoConfigPersistenceManager.class);
        doNothing().when(repoConfigPersistenceManager).setRepositoryConfigurationForRepository(repo, "blaaa");

        RepoConfig repoConfig = mock(RepoConfig.class);
        when(repoConfig.getDescription()).thenReturn("test-description");
        when(repoConfigPersistenceManager.getRepositoryConfigurationForRepository(repo)).thenReturn(repoConfig);

        servlet = new RepositoryDataServlet(soyTemplateRenderer, pageBuilderService,
                        repositoryService, permissionValidationService, repoConfigPersistenceManager);
    }

    @After
    public void tearDown() {

    }

    @Test
    public void testSomething() throws ServletException, IOException {
        String expected = "test";
        when(mockRequest.getPathInfo()).thenReturn("/test-project/test-repo");
        servlet.doGet(mockRequest, mockResponse);
        assertEquals("text/html;charset=UTF-8", mockResponse.getContentType());

    }
}
